<%@tag description="Page layout tag" pageEncoding="UTF-8" %>
<div class="nav">
    <a href="index.jsp">
        Home
    </a>
    <a href="listOfJourneys?page=0">
        Journeys
    </a>
    <a href="createJourney">
        New Journey
    </a>
</div>
<jsp:doBody/>
<div class="footer">
   <span>
       Spring &mdash; 2019
   </span>
</div>