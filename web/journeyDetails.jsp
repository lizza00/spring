<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<html>

<head>
    <link rel="stylesheet" href="css/details.css?2">
    <link rel="shortcut icon" href="images/beach.png" type="image/png">
    <link rel="stylesheet" href="css/layout.css">
    <title>Title</title>
</head>
<body>
<t:layout>
    <div class="container">
        <div>
            <div class="col-25">Special offer:</div>
            <div class="col-50">${tour.getName()}</div>
        </div>
        <div>
            <div class="col-25">Cost:</div>
            <div class="col-50">$${tour.getCost()}</div>
        </div>
        <div>
            <div class="col-25">Duration:</div>
            <div class="col-50">${tour.getDuration()} days</div>
        </div>
        <div>
            <div class="col-25">Country:</div>
            <div class="col-50">${tour.getCountry().getName()}</div>
        </div>
        <div>
            <div class="col-25">Capital:</div>
            <div class="col-50">${tour.getCountry().getCapital()}</div>
        </div>
        <div>
            <div class="col-85">${tour.getDescription()}</div>
        </div>
    </div>
</t:layout>

</body>
</html>
