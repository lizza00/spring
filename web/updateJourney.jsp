<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link rel="shortcut icon" href="images/beach.png" type="image/png">
    <link rel="stylesheet" href="css/form.css">
    <link rel="stylesheet" href="css/layout.css">
    <title>Update Journey</title>
</head>
<body>
<t:layout>
    <div class="container">

        <form method="post" action="updateJourney?id=${tour.getId()}">
            <div class="row">
                <div class="col-25">
                    <label for="name">Name of the tour:</label>
                </div>
                <div class="col-75">
                    <input type="text" id="name" name="name" value="${tour.getName()}" required></div>
            </div>
            <div class="row">
                <div class="col-25">
                    <label for="country">Choose tour`s country:</label>
                </div>
                <div class="col-75"><select name="country" id="country">
                    <c:forEach var="countryItem" items="${countryList}">
                        <c:choose>
                            <c:when test="${countryItem.getId() == tour.getCountry().getId()}">
                                <option value="${countryItem.getId()}"
                                        selected>${countryItem.getName()}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${countryItem.getId()}">${countryItem.getName()}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select></div>
            </div>
            <div class="row">
                <div class="col-25">
                    <label for="cost">Cost of tour:</label>
                </div>
                <div class="col-75"><input type="number" min="0" max="100000" step="0.01" id="cost" name="cost"
                                           value="${tour.getCost()}" required></div>
            </div>
            <div class="row">
                <div class="col-25">
                    <label for="duration">Tour`s duration(days):</label>
                </div>
                <div class="col-75"><input type="number" min="0" max="99" id="duration" name="duration"
                                           value="${tour.getDuration()}" required>
                </div>
            </div>
            <div class="row">
                <div class="col-25">
                    <label for="description">Tour`s details:</label>
                </div>
                <div class="col-75"><textarea id="description" minlength="100" maxlength="100000" name="description"
                                              required>${tour.getDescription()}</textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-25">

                </div>
                <div class="col-75"><input type="submit" value="Update">
                </div>
            </div>
        </form>
    </div>
</t:layout>
</body>
</html>
