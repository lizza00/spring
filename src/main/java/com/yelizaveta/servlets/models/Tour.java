package com.yelizaveta.servlets.models;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

@Builder
@Setter
@Getter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Tour {

    public static final int MIN_DURATION_VALUE = 1;
    public static final int MAX_DURATION_VALUE = 99;
    public static final int MIN_COST_VALUE = 0;
    public static final int MAX_COST_VALUE = 100_000;
    public static final int MIN_DESCRIPTION_LENGTH = 100;
    public static final int MAX_DESCRIPTION_LENGTH = 100_000;
    private int id;

    private String name;

    @Size(min = MIN_DESCRIPTION_LENGTH, max = MAX_DESCRIPTION_LENGTH)
    private String description;

    @Min(MIN_COST_VALUE)
    @Max(MAX_COST_VALUE)
    private double cost;

    @Min(MIN_DURATION_VALUE)
    @Max(MAX_DURATION_VALUE)
    private int duration;

    private Country country;

}
