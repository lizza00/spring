package com.yelizaveta.servlets.models;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Country {

    private int id;

    private String name;

    private String capital;

    private boolean visaRequired;
}
