package com.yelizaveta.servlets.controllers;


import com.yelizaveta.servlets.dataServices.TourDAO;
import com.yelizaveta.servlets.models.Tour;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Log4j
@WebServlet(name = "JourneyDetailsServlet", urlPatterns = {"/journeyDetails"})
public class JourneyDetailsServlet extends HttpServlet {

    private static final String ID_PARAMETER = "id";
    private static final String TOUR_PARAMETER = "tour";
    private static final String MESSAGE_PARAMETER = "message";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getServletContext().getRequestDispatcher("journeyDetails.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        log.debug("grt details");
        int id = Integer.parseInt(request.getParameter(ID_PARAMETER));
        try (TourDAO tourDAO = TourDAO.getInstance()) {
            Tour tour = tourDAO.selectTourById(id);
            request.setAttribute(TOUR_PARAMETER, tour);
        } catch (Exception e) {
            String message = "Cannot get tour";
            request.setAttribute(MESSAGE_PARAMETER, message);
            getServletContext().getRequestDispatcher("/error.jsp").forward(request, response);
            log.error(message, e);
        }
        log.debug("end of get details");
        request.getRequestDispatcher("/journeyDetails.jsp").forward(request, response);
    }
}
