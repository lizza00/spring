package com.yelizaveta.servlets.controllers;

import com.yelizaveta.servlets.dataServices.TourDAO;
import com.yelizaveta.servlets.models.Country;
import com.yelizaveta.servlets.models.Tour;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Log4j
@WebServlet(name = "CreateJourneyServlet", urlPatterns = {"/createJourney"})
public class CreateJourneyServlet extends HttpServlet {

    private static final String NAME_PARAMETER = "name";
    private static final String COST_PARAMETER = "cost";
    private static final String DURATION_PARAMETER = "duration";
    private static final String DESCRIPTION_PARAMETER = "description";
    private static final String COUNTRY_PARAMETER = "country";
    private static final String VALIDATION_MESSAGE_PARAMETER = "validationMessage";
    private static final String COUNTRY_LIST_PARAMETER = "countryList";
    private static final String ERROR_MESSAGE = "message";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String name = request.getParameter(NAME_PARAMETER);
        double cost = Double.parseDouble(request.getParameter(COST_PARAMETER));
        int duration = Integer.parseInt(request.getParameter(DURATION_PARAMETER));
        String description = request.getParameter(DESCRIPTION_PARAMETER);
        int countryId = Integer.parseInt(request.getParameter(COUNTRY_PARAMETER));
        try (TourDAO tourDAO = TourDAO.getInstance()) {
            if (tourDAO.existsTourByNameAndCountry(name, countryId)) {
                List<Country> countries = tourDAO.selectListOfCountries();
                request.setAttribute(COUNTRY_LIST_PARAMETER, countries);
                request.setAttribute(NAME_PARAMETER, name);
                request.setAttribute(COST_PARAMETER, cost);
                request.setAttribute(DURATION_PARAMETER, duration);
                request.setAttribute(DESCRIPTION_PARAMETER, description);
                request.setAttribute(COUNTRY_PARAMETER, countryId);
                request.setAttribute(VALIDATION_MESSAGE_PARAMETER, "Tour  with this name already exists in this country.");
                doGet(request,response);
            } else {
                tourDAO.insertTour(Tour.builder()
                        .name(name)
                        .cost(cost)
                        .duration(duration)
                        .country(Country.builder()
                                .id(countryId)
                                .build())
                        .description(description)
                        .build());
            }
        } catch (Exception e) {
            String message = "Cannot create journey";
            request.setAttribute(ERROR_MESSAGE, message);
            getServletContext().getRequestDispatcher("/error.jsp").forward(request, response);
            log.error(message, e);
        }
        JourneyDetailsServlet servlet = new JourneyDetailsServlet();
        servlet.doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try (TourDAO tourDAO = TourDAO.getInstance()) {
            List<Country> countries = tourDAO.selectListOfCountries();
            request.setAttribute(COUNTRY_LIST_PARAMETER, countries);
        } catch (Exception e) {
            String message = "Cannot get countries";
            request.setAttribute(ERROR_MESSAGE, message);
            getServletContext().getRequestDispatcher("/error.jsp").forward(request, response);
            log.error(message, e);
        }
        getServletContext().getRequestDispatcher("/createJourney.jsp").forward(request, response);
    }
}
