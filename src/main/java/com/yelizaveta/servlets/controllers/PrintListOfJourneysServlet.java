package com.yelizaveta.servlets.controllers;

import com.yelizaveta.servlets.dataServices.TourDAO;
import com.yelizaveta.servlets.models.Tour;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Log4j
@WebServlet(name = "PrintListOfJourneysServlet", urlPatterns = {"/listOfJourneys"})
public class PrintListOfJourneysServlet extends HttpServlet {

    private static final int COUNT_OF_PRINTED_TOURS = 10;
    private static final String PAGE_PARAMETER = "page";
    private static final String SEARCH_VALUE_PARAMETER = "searchValue";
    private static final String VALIDATION_MESSAGE_PARAMETER = "validationMessage";
    private static final String TOUR_LIST_PARAMETER = "tourList";
    private static final String NUMBER_OF_PAGE_PARAMETER = "numberOfPage";
    private static final String ERROR_MESSAGE = "message";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int numberOfPage;
        String i = request.getParameter(PAGE_PARAMETER);
        if (i == null) {
            numberOfPage = 0;
        } else {
            numberOfPage = Integer.parseInt(i);
        }
        String searchValue = request.getParameter(SEARCH_VALUE_PARAMETER);
        if (searchValue == null) {
            searchValue = StringUtils.EMPTY;
        }
        try (TourDAO tourDAO = TourDAO.getInstance()) {
            List<Tour> tours = tourDAO.selectListOfToursByName(searchValue, numberOfPage * COUNT_OF_PRINTED_TOURS, COUNT_OF_PRINTED_TOURS);
            if (tours.size() == 0) {
                request.setAttribute(VALIDATION_MESSAGE_PARAMETER, "There is no such tours.");
                getServletContext().getRequestDispatcher("/listOfJourneys.jsp").forward(request, response);
            }
            request.setAttribute(TOUR_LIST_PARAMETER, tours);
            request.setAttribute(NUMBER_OF_PAGE_PARAMETER, numberOfPage);
        } catch (Exception e) {
            String message = "Cannot get tours";
            request.setAttribute(ERROR_MESSAGE, message);
            getServletContext().getRequestDispatcher("/error.jsp").forward(request, response);
            log.error(message, e);
        }

        getServletContext().getRequestDispatcher("/listOfJourneys.jsp").forward(request, response);
    }
}
