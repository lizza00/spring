package com.yelizaveta.servlets.exceptions;

public class NotFoundIdException extends Exception{
    public NotFoundIdException(String message){
        super(message);
    }
}
