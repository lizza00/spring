package com.yelizaveta.servlets.dataServices;


import com.yelizaveta.servlets.exceptions.NotFoundIdException;
import com.yelizaveta.servlets.models.Country;
import com.yelizaveta.servlets.models.Tour;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Log4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TourDAO implements AutoCloseable {

    private static final String TOUR_TABLE_NAME = "tour";
    private static final String COUNTRY_TABLE_NAME = "country";
    private static final String SELECT_ALL_FROM_COUNTRY_QUERY = String.format("select * from %s;", COUNTRY_TABLE_NAME);
    private static final String ID_TOUR_COLUMN_NAME = "id";
    private static final String NAME_TOUR_COLUMN_NAME = "name";
    private static final String ID_COUNTRY_COLUMN_NAME = "id";
    private static final String NAME_COUNTRY_COLUMN_NAME = "name";
    private static final String CAPITAL_COUNTRY_COLUMN_NAME = "capital";
    private static final String COST_TOUR_COLUMN_NAME = "cost";
    private static final String DURATION_TOUR_COLUMN_NAME = "duration";
    private static final String COUNTRY_ID_TOUR_COLUMN_NAME = "countryId";
    private static final String DESCRIPTION_TOUR_COLUMN_NAME = "description";
    private static final String INSERT_INTO_TOUR_QUERY = "insert into %s(%s, %s, %s, %s, %s) values('%s', %f, %d, %d, '%s')";
    private static final String UPDATE_TOUR_QUERY = "update %s set %s = '%s', %s = %f, %s = %d, %s = %d, %s ='%s' where %s  =  %d ;";
    private static final String SELECT_ALL_FROM_TOUR_QUERY = "select * from %s where %s = '%s' and %s = %d;";
    private static final String SELECT_TOUR_BY_ID_QUERY = "select t.*, c.%s, c.%s from %s as t inner join %s as c on t.%s = c.%s where t.%s = %d;";
    private static final String SELECT_TOURS_BY_NAME_QUERY = "select t.*, c.%s, c.%s from %s as t inner join %s as c on t.%s = c.%s where t.%s like '%%%s%%' order by t.%s limit %d, %d;";
    private static TourDAO instance;
    private static Connection connection;
    private static Statement statement;

    public static TourDAO getInstance() throws ClassNotFoundException, SQLException {
        if (instance == null) {
            instance = new TourDAO();
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:C:/Users/Acer/IdeaProjects/_123/src/main/resources/com/globallogic/mykolayiv/basecamp/servlets/db/TourDB.db");
            statement = connection.createStatement();
        }
        return instance;
    }

    public List<Tour> selectListOfToursByName(String searchValue, int shift, int count) throws SQLException {
        statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(String.format(SELECT_TOURS_BY_NAME_QUERY,
                NAME_COUNTRY_COLUMN_NAME, CAPITAL_COUNTRY_COLUMN_NAME,
                TOUR_TABLE_NAME, COUNTRY_TABLE_NAME, COUNTRY_ID_TOUR_COLUMN_NAME,
                ID_COUNTRY_COLUMN_NAME, NAME_TOUR_COLUMN_NAME, searchValue,
                NAME_TOUR_COLUMN_NAME, shift, count));
        List<Tour> tours = new ArrayList<>();
        while (resultSet.next()) {
            tours.add(buildTour(resultSet.getInt(1), resultSet.getString(2),
                    resultSet.getDouble(3), resultSet.getInt(4), resultSet.getInt(5),
                    resultSet.getString(7), resultSet.getString(8), resultSet.getString(6)));
        }
        log.info("Tours by name selected");
        return tours;
    }

    public Tour selectTourById(int id) throws SQLException, NotFoundIdException {
        ResultSet resultSet = statement.executeQuery(String.format(
                SELECT_TOUR_BY_ID_QUERY, NAME_COUNTRY_COLUMN_NAME, CAPITAL_COUNTRY_COLUMN_NAME,
                TOUR_TABLE_NAME, COUNTRY_TABLE_NAME, COUNTRY_ID_TOUR_COLUMN_NAME,
                ID_COUNTRY_COLUMN_NAME, ID_TOUR_COLUMN_NAME, id));
        if (resultSet.next()) {
            log.info("Tour by id is selected");
            return buildTour(resultSet.getInt(1), resultSet.getString(2),
                    resultSet.getDouble(3), resultSet.getInt(4), resultSet.getInt(5),
                    resultSet.getString(7), resultSet.getString(8), resultSet.getString(6));
        }
        log.debug(id + " not found");
        throw new NotFoundIdException(id + " not found.");
    }

    public boolean existsTourByNameAndCountry(String name, int countryId) throws SQLException {
        log.info("Tour by name and country is selected");
        return statement.executeQuery(String.format(
                SELECT_ALL_FROM_TOUR_QUERY, TOUR_TABLE_NAME, NAME_TOUR_COLUMN_NAME,
                name, COUNTRY_ID_TOUR_COLUMN_NAME, countryId))
                .next();
    }

    public List<Country> selectListOfCountries() throws SQLException {
        List<Country> countries = new ArrayList<>();
        ResultSet resultSet = statement.executeQuery(SELECT_ALL_FROM_COUNTRY_QUERY);
        while (resultSet.next()) {
            countries.add(Country.builder()
                    .id(resultSet.getInt(1))
                    .name(resultSet.getString(2))
                    .capital(resultSet.getString(3))
                    .build());
        }
        log.info("Countries selected");
        return countries;
    }

    public void insertTour(Tour tour) throws SQLException {
        statement.execute(String.format(INSERT_INTO_TOUR_QUERY, TOUR_TABLE_NAME,
                NAME_TOUR_COLUMN_NAME, COST_TOUR_COLUMN_NAME, DURATION_TOUR_COLUMN_NAME,
                COUNTRY_ID_TOUR_COLUMN_NAME, DESCRIPTION_TOUR_COLUMN_NAME, tour.getName(),
                tour.getCost(), tour.getDuration(), tour.getCountry().getId(), tour.getDescription()));
        log.info("Tour is inserted");
    }

    public void updateTour(Tour tour) throws SQLException {
        statement.execute(String.format(UPDATE_TOUR_QUERY, TOUR_TABLE_NAME, NAME_TOUR_COLUMN_NAME,
                tour.getName(), COST_TOUR_COLUMN_NAME, tour.getCost(), DURATION_TOUR_COLUMN_NAME,
                tour.getDuration(), COUNTRY_ID_TOUR_COLUMN_NAME, tour.getCountry().getId(),
                DESCRIPTION_TOUR_COLUMN_NAME, tour.getDescription(), ID_TOUR_COLUMN_NAME, tour.getId()));
        log.info("Tour is updated");
    }

    private Tour buildTour(int id, String name, double cost, int duration, int countryId,
                           String countryName, String capital, String description) {
        return Tour.builder()
                .id(id)
                .name(name)
                .cost(cost)
                .duration(duration)
                .country(Country.builder()
                        .id(countryId)
                        .name(countryName)
                        .capital(capital)
                        .build())
                .description(description)
                .build();
    }

    @Override
    public void close() throws Exception {
        statement.close();
        connection.close();
        instance = null;
    }
}
